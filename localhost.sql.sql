-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Tempo de geração: 03-Ago-2022 às 14:44
-- Versão do servidor: 5.6.25-log
-- versão do PHP: 7.3.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `imoveis_3e_gb_2022`
--
DROP DATABASE IF EXISTS `imoveis_3e_gb_2022`;
CREATE DATABASE IF NOT EXISTS `imoveis_3e_gb_2022` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `imoveis_3e_gb_2022`;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cliente`
--

CREATE TABLE `cliente` (
  `id` int(11) NOT NULL,
  `sexo` enum('masculino','feminino','não informado') NOT NULL,
  `data_nascimento` date NOT NULL,
  `data_cadastro` date NOT NULL,
  `orientacao` varchar(30) NOT NULL,
  `nome_social` varchar(40) DEFAULT NULL,
  `raca` varchar(20) NOT NULL,
  `deficiencia` tinyint(1) NOT NULL,
  `descricao_deficiencia` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estrutura da tabela `contrato_imobiliaria_cliente`
--

CREATE TABLE `contrato_imobiliaria_cliente` (
  `id` int(11) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `id_contrato_imobiliaria` int(11) NOT NULL,
  `data_inicio` date NOT NULL,
  `data_termino` date NOT NULL,
  `valor_combinado` decimal(10,0) NOT NULL,
  `tipo` enum('aluguel','venda') NOT NULL,
  `fiador` varchar(100) NOT NULL,
  `ativo` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estrutura da tabela `contrato_imobiliaria_proprietario`
--

CREATE TABLE `contrato_imobiliaria_proprietario` (
  `id` int(11) NOT NULL,
  `id_imovel` int(11) NOT NULL,
  `id_imobiliaria` int(11) NOT NULL,
  `data_inicial` date NOT NULL,
  `data_termino` date NOT NULL,
  `aluguel` decimal(10,0) NOT NULL,
  `venda` decimal(10,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estrutura da tabela `funcionario`
--

CREATE TABLE `funcionario` (
  `id` int(11) NOT NULL,
  `data_admissao` date NOT NULL,
  `salario` decimal(10,0) NOT NULL,
  `cargo` enum('corretor','agente','gestor','manutencao') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estrutura da tabela `imobiliaria`
--

CREATE TABLE `imobiliaria` (
  `id` int(11) NOT NULL,
  `cnpj` varchar(18) NOT NULL,
  `ie` varchar(20) NOT NULL,
  `razao_social` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estrutura da tabela `imovel`
--

CREATE TABLE `imovel` (
  `id` int(11) NOT NULL,
  `descricao` text NOT NULL,
  `rua` varchar(100) NOT NULL,
  `numero` varchar(10) NOT NULL,
  `bairro` varchar(30) NOT NULL,
  `cep` varchar(15) NOT NULL,
  `cidade` varchar(30) NOT NULL,
  `uf` varchar(2) NOT NULL,
  `cozinhas` int(11) NOT NULL,
  `salas` int(11) NOT NULL,
  `quartos` int(11) NOT NULL,
  `banheiros` int(11) NOT NULL,
  `tem_garagem` tinyint(1) NOT NULL,
  `vagas` int(11) NOT NULL DEFAULT '0',
  `complemento` varchar(100) NOT NULL,
  `area_construida` int(11) NOT NULL,
  `area_terreno` int(11) NOT NULL,
  `id_proprietario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pessoa`
--

CREATE TABLE `pessoa` (
  `id` int(11) NOT NULL,
  `nome` varchar(150) NOT NULL,
  `telefone` varchar(16) DEFAULT NULL,
  `rua` varchar(100) NOT NULL,
  `numero` varchar(10) NOT NULL,
  `bairro` varchar(50) NOT NULL,
  `cep` varchar(11) NOT NULL,
  `cidade` varchar(50) NOT NULL,
  `uf` varchar(2) NOT NULL,
  `tipo` enum('fisica','juridica') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario`
--

CREATE TABLE `usuario` (
  `id` int(11) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(30) NOT NULL,
  `ativo` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `cliente`
--
ALTER TABLE `cliente`
  ADD KEY `id` (`id`);

--
-- Índices para tabela `contrato_imobiliaria_cliente`
--
ALTER TABLE `contrato_imobiliaria_cliente`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_locador` (`id_cliente`),
  ADD KEY `id_contrato_imobiliaria` (`id_contrato_imobiliaria`);

--
-- Índices para tabela `contrato_imobiliaria_proprietario`
--
ALTER TABLE `contrato_imobiliaria_proprietario`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_imovel` (`id_imovel`),
  ADD KEY `id_imobiliaria` (`id_imobiliaria`);

--
-- Índices para tabela `funcionario`
--
ALTER TABLE `funcionario`
  ADD KEY `id` (`id`);

--
-- Índices para tabela `imobiliaria`
--
ALTER TABLE `imobiliaria`
  ADD UNIQUE KEY `cnpj` (`cnpj`),
  ADD KEY `id` (`id`);

--
-- Índices para tabela `imovel`
--
ALTER TABLE `imovel`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_proprietario` (`id_proprietario`);

--
-- Índices para tabela `pessoa`
--
ALTER TABLE `pessoa`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `usuario`
--
ALTER TABLE `usuario`
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `password` (`password`),
  ADD KEY `id` (`id`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `contrato_imobiliaria_cliente`
--
ALTER TABLE `contrato_imobiliaria_cliente`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `contrato_imobiliaria_proprietario`
--
ALTER TABLE `contrato_imobiliaria_proprietario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `imovel`
--
ALTER TABLE `imovel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `pessoa`
--
ALTER TABLE `pessoa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Restrições para despejos de tabelas
--

--
-- Limitadores para a tabela `cliente`
--
ALTER TABLE `cliente`
  ADD CONSTRAINT `cliente_ibfk_1` FOREIGN KEY (`id`) REFERENCES `usuario` (`id`) ON UPDATE CASCADE;

--
-- Limitadores para a tabela `contrato_imobiliaria_cliente`
--
ALTER TABLE `contrato_imobiliaria_cliente`
  ADD CONSTRAINT `contrato_imobiliaria_cliente_ibfk_1` FOREIGN KEY (`id_contrato_imobiliaria`) REFERENCES `contrato_imobiliaria_proprietario` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `contrato_imobiliaria_cliente_ibfk_2` FOREIGN KEY (`id_cliente`) REFERENCES `cliente` (`id`) ON UPDATE CASCADE;

--
-- Limitadores para a tabela `contrato_imobiliaria_proprietario`
--
ALTER TABLE `contrato_imobiliaria_proprietario`
  ADD CONSTRAINT `contrato_imobiliaria_proprietario_ibfk_1` FOREIGN KEY (`id_imovel`) REFERENCES `imovel` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `contrato_imobiliaria_proprietario_ibfk_2` FOREIGN KEY (`id_imobiliaria`) REFERENCES `imobiliaria` (`id`) ON UPDATE CASCADE;

--
-- Limitadores para a tabela `funcionario`
--
ALTER TABLE `funcionario`
  ADD CONSTRAINT `funcionario_ibfk_1` FOREIGN KEY (`id`) REFERENCES `usuario` (`id`) ON UPDATE CASCADE;

--
-- Limitadores para a tabela `imobiliaria`
--
ALTER TABLE `imobiliaria`
  ADD CONSTRAINT `imobiliaria_ibfk_1` FOREIGN KEY (`id`) REFERENCES `pessoa` (`id`) ON UPDATE CASCADE;

--
-- Limitadores para a tabela `imovel`
--
ALTER TABLE `imovel`
  ADD CONSTRAINT `imovel_ibfk_1` FOREIGN KEY (`id_proprietario`) REFERENCES `cliente` (`id`) ON UPDATE CASCADE;

--
-- Limitadores para a tabela `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `usuario_ibfk_1` FOREIGN KEY (`id`) REFERENCES `pessoa` (`id`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
