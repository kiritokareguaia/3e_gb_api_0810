const { Op } = require('sequelize')

const database = require('../models')



class ClientesController {

    static async buscarTodosOsClientes(req, res) {

        try {

            const todosOsClientes = await database.Cliente.findAll()

            return res.status(200).json(todosOsClientes)

        } catch (error) {

            return res.status(500).json(error.message)

        }

    }



    static async criarUmCliente (req, res) {

        const novoUsuario = req.body

        try{

            const novoCienteCriado = await database.Usuario.create(novoCliente)

            return res.status(200).json(novoCienteCriado)

            } catch (error) {

                return res.status(500).json(error.message)

            }

    }



    static async buscarClientePeloNomeSocial(req, res) {

        const { nome } = req.params

        try {         
            const todosOsClientes = await database.Cliente.findAll({

                where: {

                    nome_social: {

                        [Op.like]: '%${nome}%'

                    }

                }

            })

            return res.status(200).json(todosOsClientes)

        } catch (error) {

            return res.status(500).json(error.message)

        }

    }

}



module.exports = ClientesController