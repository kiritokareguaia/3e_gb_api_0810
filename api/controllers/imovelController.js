const { Op } = require('sequelize')
const database = require('../models')



class imovelController{
    static async CriarImovel (req, res) {
        const novoImovel = req.body
        try{
            const novoImovel = await database.imoveis.create(novoImovel)
            return res.status(200).json(novoImovel)
        } catch (error) {
            return res.status(500).json(error.message)
        }
    }

    static async bucarTodosOsImoveis (req, res){
        try{
            const todosOsImoveis = await database.imoveis.findAll()
            return res.status(200).json(todosOsImoveis)
        } catch (error) {
            return res.status(500).json(error.message)
        }
    }
    
    static async buscarTodosOsImoveisPelaCidade (req, res){
        try{
            const imoveisPelaCidade = await database.imoveis.findAll()
            return res.status(200).json(imoveisPelaCidade)
        }catch (error) {
            return res.status(500).json(error.message)
        }
    }

    static async deletarUmImovel(req, res){
        const {id} = req.params
        try{
            await database.imoveis.destroy({
                where: {
                    id: Number(id)
                }
            })
            return res.status(200).json({mensagem: `imovel ${id} excluido com sucesso`})
        } catch (error){
            return res.status(500).json(error.message)
        }
        
    }
}

module.exports = imovelController