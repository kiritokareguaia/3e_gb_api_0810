const { Op } = require('sequelize')
const database = require('../models')

class cont_imo_proprietarioController {

    static async novoProprietario (req, res) {
        try {
            const novoProprietario = await database.cont_imo_proprietario.create(novoProprietario)
            return res.status(200).json(novoProprietario)
        } catch (error) {
            return res.status(500).json(error.message)
        }
    }

    static async deletarConratoProprietario(req, res) {
        const {id} = req.params
        try{
            await database.cont_imo_proprietario.destroy({
                where: {
                    id: Number(id)
                }
            })
            return res.status(200).json({mensagem: `contrato do proprietario ${id} excluido com sucesso`})
        } catch (error) {
            return res.status(500).json(error.message)
        }
    }

    static async bucarTodosOsContratosProprietarios (req, res) {
        try{
            const contratoProprietario = await database.cont_imo_proprietario.findAll()
            return res.status(200).json(contratoProprietario)
        } catch (error) {
            return res.status(500).json(error.message)
        }
        
    }

}

module.exports = cont_imo_proprietarioController