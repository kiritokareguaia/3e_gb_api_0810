const { Op } = require('sequelize')

const database = require('../models')



class  funcionarioController {

    static async buscarTodosOsFuncionarios (req, res) {

    try{

        const todosOsFuncionarios = await database.funcionario.findAll()

        return res.status(200).json(todosOsFuncionarios)

    } catch (error) {

        return res.status(500).json(error.message)

    }



}

    static async criarUmFuncionario (req, res) {

    const novoFuncionario = req.body

    try {

        const novoFuncionarioCriado = await database.funcionario.creat(novoFuncionario)

        return res.status(200).json(novoFuncionarioCriado)

    } catch (error){

        return res.status(500).json(error.message)

    }

}

    static async  deletarUmFuncionario (req, res) {

    const { id } = req.params

    try {

        await database.funcionario.destroy({

            where: {

                id: Number(id)



            }

        })

        return res.status(200).json({

            mensagem: `Funcionario ${id} excluido com sucesso!`

        })

        }catch (error){

            return res.status(500).json(error.message)

        }

    }

}