const { Op } = require('sequelize')
const database = require('../models')

class PessoaController {
    static async buscarTodasAsPessoas(req, res) {
        try {
            const todasAsPessoas = await database.Pessoa.findAll()
            return res.status(200).json(todasAsPessoas)
        } catch (error) {
            return res.status(500).json(error.message)
        }
    }

    static async criarUmaPessoa(req, res) {
        const novaPessoa = req.body
        try {
            const novaPessoaCriada = await database.Pessoa.create(novaPessoa)
            return res.status(200).json(novaPessoaCriada)
        } catch (error) {
            return res.status(500).json(error.message)
        }
    }

    static async buscarPessoaPeloNome(req, res) {
        const { nome } = req.params
        try {
            const todasAsPessoas = await database.Pessoa.findAll({
                where: {
                    nome: {
                        [Op.like]: '%${nome}%'
                    }
                }
            })
            return res.status(200).json(todasAsPessoas)
        } catch (error) {
            return res.status(500).json(error.message)
        }
    }
    static async buscarPessoaPeloNomeECidade(req, res) {
        const {nome, cidade} = req.params
        try{
            const todasAsPessoas = await database.Pessoa.findAll({
                where: {
                    nome: nome,
                    cidade: cidade
                }
            })
            return res.status(200).json(todasAsPessoas)
        } catch (error) {
            return res.status(500).json(error.message)
        }
    }
}

module.exports = PessoaController
