const { Op } = require('sequelize')
const database = require('../models')

class imobiliariaController {
    static async buscarTodasAsImobiliarias (req, res) {
        try {
            const todasAsImobiliarias = await database.Imobiliaria.findAll()
            return res.status(200).json(todasAsImobiliarias)
        } catch (error) {
            return res.status(500).json(error.message)
        }
    }

    static async deletarImobiliaria(req, res) {
        const { id } = req.params
        try{
            await database.Imobiliaria.destroy({
                where: {
                    id: Number(id)
                }
            })
            return res.status(200).json({mensagem: `imobiliaria ${id} excluida com sucesso`})
        } catch (error){
            return res.status(500).json(error.message)
        }
    }

    static async atualizarImobiliaria(req, res){
        const { id } = req.params
        const novosDados = req.body
        try {
            await database.Imobiliaria.update(novosDados, {
                where: {
                    id: Number(id)
                }
            })
            const imobiliariaAtualizado = await database.Imobiliaria.findOne({
                where: {
                    id: Number(id)
                }
            })
            return res.status(200).json(imobiliariaAtualizado)
        }catch (error) {
            return res.status(500).json(error.message)
        }
    }
}
module.exports = imobiliariaController