const { Op } = require('sequelize')
const database = require('../models')

class cont_imo_clienteController {
    static async buscarTodosOsContratos (req, res) {
        try {
            const todosOsContratos = await database.contrato_imobiliaria_cliente.findAll()
            return res.status(200).json(todosOsContratos)
        } catch (error) {
            return res.status(500).json(error.message)
        }
    }

    static async criarUmContrato (req, res) {
        const novoContrato = req.body
        try{
            const novoContratoCriado = await database.contrato_imobiliaria_cliente.create(novoUsuario)
            return res.status(200).json(novoContratoCriado)
            } catch (error) {
                return res.status(500).json(error.message)
            }
    }

    static async deletarUmContrato(req, res) {
        const { id } = req.params
        try{
            await database.contrato_imobiliaria_cliente.destroy({
                where: {
                    id: Number(id)
                }
            })
            return res.status(200).json({mensagem: `contrato ${id} excluido com sucesso`})
        } catch (error){
            return res.status(500).json(error.message)
        }
    }
    }
    module.exports = cont_imo_clienteController