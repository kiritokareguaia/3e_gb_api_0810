const { Op } = require('sequelize')
const database = require('../models')

class UsuariosController {
    static async logar (req, res) {
        const usuario = req.body
        try {
            const usuarioLogado = await database.Usuario.findOne({
                include: {
                    model: database.Pessoa,
                    required: true
                },
                where: {
                    [Op.and]: {
                        username: usuario.username,
                        password: usuario.password,
                        ativo: true
                    }
                }
            })
            
            const usuarioRetornado = {
                nome: null,
                username: null
            }

            if (usuarioLogado === null){
                return res.status(401).json({
                    "mensagem": "Usuário não autorizado",
                    "status": "block",
                    "dados": usuarioRetornado
                })
            }
            
            usuarioRetornado.nome = usuarioLogado.Pessoa.nome
            usuarioRetornado.username = usuarioLogado.username

            return res.status(200).json({
                "mensagem": "Usuário autorizado",
                "status": "ok",
                "dados": usuarioRetornado
            })
        } catch (error) {
            return res.status(500).json({
                "mensagem": "Erro ao logar",
                "status": "error",
                "dados": error
            })
        }
    }

    static async buscarTodosOsUsuarios (req, res) {
        try {
            const todosOsUsuarios = await database.Usuario.findAll()
            return res.status(200).json(todosOsUsuarios)
        } catch (error) {
            return res.status(500).json(error.message)
        }
    }

    static async criarUmUsuario (req, res) {
        const novoUsuario = req.body
        try {
            const novoUsuarioCriado = await database.Usuario.create(novoUsuario)
            return res.status(200).json(novoUsuarioCriado)
        } catch (error) {
            return res.status(500).json(error.message)
        }
    }

    static async criarUmUsuarioEPessoa (req, res){
        const usuario = req.body

        try{
            const novaPessoa = await database.Pessoa.create(usuario)

            try{
                usuario.id = novaPessoa.id

                const novoUsuario = await database.Usuario.create(usuario)

                return res.status(200).json(usuario)
            }catch(error){
                return res.status(500).json(error)
            }   
        }catch(error){
            return res.status(500).json(error)
        }
    }

    static async buscarTodosOsUsuariosCompletos (req, res) {
        try {
            const todosOsUsuarios = await database.Pessoa.findAll({
                include: {
                    model: database.Usuario,
                    required: true
                }
            })
            return res.status(200).json(todosOsUsuarios)
        } catch (error) {
            return res.status(500).json(error.message)
        }
    }

    static async buscarOsUsuariosCompletosPeloNome (req, res) {
        const { nome } = req.params
        try {
            const todosOsUsuarios = await database.Pessoa.findAll({
                include: { 
                    model: database.Usuario, 
                    required: true 
                },
                where: {
                    nome: {
                        [Op.substring]: nome
                    }
                }
            })
            return res.status(200).json(todosOsUsuarios)
        } catch (error) {
            return res.status(500).json(error.message)
        }
    }

    static async deletarUmUsuario(req, res) {
        const { id } = req.params
        try{
            await database.Usuario.destroy({
                where: {
                    id: Number(id)
                }
            })
            return res.status(200).json({
                mensagem: `Usuário ${id} excluído com sucesso!`
            })
        }catch (error){
            return res.status(500).json(error.message)
        }
    }

    static async atualizarUmUsuario(req, res) {
        const { id } = req.params
        const novosDados = req.body
        try{
            await database.Usuario.update(novosDados, {
                where: {
                    id: Number(id)
                }
            })
            const usuarioAtualizado = await database.Usuario.findOne({
                where: {
                    id: Number(id)
                }
            })
            return res.status(200).json(usuarioAtualizado)
        }catch (error) {
            return res.status(500).json(error.message)
        }
    }
}

module.exports = UsuariosController
