'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Funcionario extends Model {
    static associate(models) {
      Funcionario.belongsTo(models.Usuario, {
        foreignKey: 'id'
      })
    }
  }
  Funcionario.init({
    data_admissao: DataTypes.DATE,
    salario: DataTypes.FLOAT,
    cargo: DataTypes.ENUM({
      values: ['corretor','agente','gestor','manutencao']
    })
  }, {
    sequelize,
    modelName: 'Funcionario',
    tableName: 'funcionario'
  });
  return Funcionario;
};
