'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Imobiliaria extends Model {
    static associate(models) {
      Imobiliaria.belongsTo(models.Pessoa, {
        foreignKey: 'id'
      })
      Imobiliaria.hasMany(models.ContratoImobiliariaProprietario, {
        foreignKey: 'id_imobiliaria'
      })
    }
  }
  Imobiliaria.init({
    cnpj: DataTypes.STRING(18),
    ie: DataTypes.STRING(20),
    rasao_social: DataTypes.STRING(50)
  }, {
    sequelize,
    modelName: 'Imobiliaria',
    tableName: 'imobiliaria'
  });
  return Imobiliaria;
};
