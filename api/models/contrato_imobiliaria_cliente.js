'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class contrato_imobiliaria_cliente extends Model {
    static associate(models){
      contrato_imobiliaria_cliente.belongsTo(models.Cliente, {
        foreingnKey: 'id'
      })
      contrato_imobiliaria_cliente.belongsTo(models.ContratoImobiliariaProprietario, {
        foreingnKey: 'id'
      })
    }
  }
  contrato_imobiliaria_cliente.init({
    id_cliente: DataTypes.INTEGER(11),
    id_contrato_imobiliaria:DataTypes.INTEGER(11),
    data_inicio: DataTypes.DATE,
    data_termino: DataTypes.DATE,
    valor_combinado: DataTypes.DECIMAL(10,0),
    tipo: DataTypes.ENUM({
      values: ['aluguel', 'venda']
   }),
   fiador:DataTypes.STRING(100),
   ativo:DataTypes.BOOLEAN
  }, {
    sequelize,
    modelName: 'contrato_imobiliaria_cliente',
    tableName: 'contrato_imobiliaria_cliente'
  });
  return contrato_imobiliaria_cliente;
};