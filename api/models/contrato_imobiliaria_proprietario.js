'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class ContratoImobiliariaProprietario extends Model {
    
    static associate(models) {
      ContratoImobiliariaProprietario.belongsTo(models.Imobiliaria, {
        foreignKey: 'id'
      })
      ContratoImobiliariaProprietario.belongsTo(models.Imovel, {
        foreignKey: 'id'
      })
      ContratoImobiliariaProprietario.belongsTo(models.contrato_imobiliaria_cliente, {
        foreignKey: 'id_contrato_imobiliaria'
      })
    }
  }
  ContratoImobiliariaProprietario.init({
    id_imovel: DataTypes.INTEGER(11),
    id_imobiliaria: DataTypes.INTEGER(11),
    data_inicial: DataTypes.DATE,
    data_termino: DataTypes.DATE,
    alugel: DataTypes.DECIMAL(10,0),
    venda: DataTypes.DECIMAL(10,0)

  }, {
    sequelize,
    modelName: 'ContratoImobiliariaProprietario',
    tableName: 'contrato_imobiliaria_proprietario'
  });
  return ContratoImobiliariaProprietario;
};