'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Imovel extends Model {
    static associate(models) {
      Imovel.belongsTo(models.Cliente, {
        foreignKey: 'id'
      })
    }
  }
  Imovel.init({
   descricao: DataTypes.TEXT,
   rua: DataTypes.STRING(100),
   numero: DataTypes.STRING(10),
   bairro: DataTypes.STRING(30),
   cep: DataTypes.STRING(15),
   cidade: DataTypes.STRING(30),
   uf: DataTypes.STRING(2),
   cozinhas:DataTypes.INTEGER(11), 
   salas:DataTypes.INTEGER(11),
   quartos:DataTypes.INTEGER(11),
   banheiros:DataTypes.INTEGER(11),
   tem_garagem: DataTypes.BOOLEAN,
   vagas:DataTypes.INTEGER(11),
   complemento:DataTypes.STRING(100),
   area_construida:DataTypes.INTEGER(11),
   area_terreno:DataTypes.INTEGER(11),
   id_proprietario:DataTypes.INTEGER(11)
  }, {
    sequelize,
    modelName: 'Imovel',
    tableName: 'imovel'
  });
  return Imovel;
};