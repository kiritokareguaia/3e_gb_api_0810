'use strict';
const {
  Model
} = require('sequelize');
const cliente = require('./cliente');
module.exports = (sequelize, DataTypes) => {
  class Usuario extends Model {
    static associate(models) {
      Usuario.belongsTo(models.Pessoa, {
        foreignKey: 'id'
      })
      Usuario.hasOne(models.Funcionario, {
        foreignKey: 'id'
      })
      Usuario.hasOne(models.Cliente, {
        foreignKey: 'id'
      })
    }
  }
  Usuario.init({
    username: DataTypes.STRING(30),
    password: DataTypes.STRING(30),
    ativo: DataTypes.BOOLEAN
  }, {
    sequelize,
    modelName: 'Usuario',
    tableName: 'usuario'
  });
  return Usuario;
};