'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Pessoa extends Model {
    static associate(models) {
      Pessoa.hasOne(models.Usuario, {
        foreignKey: 'id'
      })
      Pessoa.hasOne(models.Imobiliaria, {
        foreignKey: 'id'
      })
    }
  }
  Pessoa.init({
    nome: DataTypes.STRING(150),
    telefone: DataTypes.STRING(16),
    rua: DataTypes.STRING(100),
    numero: DataTypes.STRING(10),
    bairro: DataTypes.STRING(50),
    cep: DataTypes.STRING(11),
    cidade: DataTypes.STRING(50),
    uf: DataTypes.STRING(2),
    tipo: DataTypes.ENUM({
      values: ['física', 'jurídica']
   })
  }, {
    sequelize,
    modelName: 'Pessoa',
    tableName: 'pessoa'
  });
  return Pessoa;
};