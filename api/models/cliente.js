'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Cliente extends Model {
    static associate(models) {
      Cliente.belongsTo(models.Usuario, {
        foreignKey: 'id'
      })
      Cliente.hasMany(models.Imovel, {
        foreignKey: 'id_proprietario'
      })
    }
  }
  Cliente.init({
    sexo: DataTypes.ENUM({
      values: ['masculino', 'feminino', 'não informado']
   }),
    data_nascimento: DataTypes.DATEONLY,
    data_cadastro: DataTypes.DATEONLY,
    orientacao: DataTypes.STRING(30),
    nome_social: DataTypes.STRING(40),
    raca: DataTypes.STRING(20),
    deficiencia: DataTypes.BOOLEAN,
    descricao_deficiencia: DataTypes.TEXT
  }, {
    sequelize,
    modelName: 'Cliente',
    tableName: 'cliente'
  });
  return Cliente;
};
