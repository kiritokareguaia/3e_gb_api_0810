'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('imobiliaria', {
      id: {
        allowNull: false,
        autoIncrement: false,
        primaryKey: true,
        type: Sequelize.INTEGER,
        references: { model:'pessoa', key: 'id'}
      },
      cnpj: {
        type: Sequelize.STRING(18),
        allowNull: false
      },
      ie: {
        type: Sequelize.STRING(20),
        allowNull: false
      },
      rasao_social: {
        type: Sequelize.STRING(50),
        allowNull: false
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('imobiliaria');
  }
};
