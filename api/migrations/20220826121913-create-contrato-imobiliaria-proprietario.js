'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('contrato_imobiliaria_proprietarios', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      id_imovel: {
        type: Sequelize.INTEGER,
        allowNull:false,
        references: {model: 'imovel', key: 'id' }
      },
      id_imobiliaria: {
        type: Sequelize.INTEGER,
        allowNull:false,
        references: {model: 'imobiliaria', key: 'id' }
      },
      data_inicial: {
        allowNull:false,
        type: Sequelize.DATE
      },
      data_termino: {
        allowNull:false,
        type: Sequelize.DATE
      },
      alugel: {
        allowNull:false,
        type: Sequelize.DECIMAL(10,0)
      },
      venda: {
        allowNull:false,
        type: Sequelize.DECIMAL(10,0)
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('contrato_imobiliaria_proprietario');
  }
};