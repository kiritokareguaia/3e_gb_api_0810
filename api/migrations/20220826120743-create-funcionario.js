'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('funcionario', {
      id: {
        allowNull: false,
        autoIncrement: false,
        primaryKey: true,
        type: Sequelize.INTEGER,
        references: { model: 'usuario', key: 'id'}
      },
      data_admissao: {
        type: Sequelize.DATE,
        allowNull: false
      },
      salario: {
        type: Sequelize.FLOAT,
        allowNull: false
      },
      cargo: {
        type: Sequelize.ENUM({ 
          values: ['corretor', 'agente', 'gestor', 'manutencao']
      }),
        allowNull: false
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('funcionario');
  }
};
