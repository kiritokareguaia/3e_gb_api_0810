'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('imovel', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      descricao: {
        allowNull:false,
        type: Sequelize.TEXT
      },
      rua: {
        allowNull:false,
        type: Sequelize.STRING(100)
      },
      numero: {
        allowNull:false,
        type: Sequelize.STRING(10)
      },
      bairro: {
        allowNull:false,
        type: Sequelize.STRING(30)
      },
      cep: {
        allowNull:false,
        type: Sequelize.STRING(15)
      },
      cidade: {
        allowNull:false,
        type: Sequelize.STRING(30)
      },
      uf: {
        allowNull:false,
        type: Sequelize.STRING(2)
      },
      cozinhas: {
        allowNull:false,
        type: Sequelize.INTEGER(11)
      },
      salas: {
        allowNull:false,
        type: Sequelize.INTEGER(11)
      },
      quartos: {
        allowNull:false,
        type: Sequelize.INTEGER(11)
      },
      banheiros: {
        allowNull:false,
        type: Sequelize.INTEGER(11)
      },
      tem_garagem: {
        allowNull:false,
        type: Sequelize.BOOLEAN
      },
      vagas: {
        allowNull:false,
        type: Sequelize.INTEGER(11)
      },
      complemento: {
        allowNull:false,
        type: Sequelize.STRING(100)
      },
      area_construida: {
        allowNull:false,
        type: Sequelize.INTEGER(11)
      },
      area_terreno: {
        allowNull:false,
        type: Sequelize.INTEGER(11)
      },
      id_proprietario: {
        allowNull:false,
        type: Sequelize.INTEGER(11),
        references: { model: 'cliente', key: "id" }
      },

      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('imovel');
  }
};