'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('cliente', {
      id: {
        allowNull: false,
        autoIncrement: false,
        primaryKey: true,
        type: Sequelize.INTEGER,
        references: { model: 'usuario', key: 'id'}
      },
      sexo: {
        type: Sequelize.ENUM({ 
          values: ['masculino', 'feminino', 'não informado']
      }),
          allowNull: false
      },
      data_nascimento: {
        type: Sequelize.DATEONLY,
        allowNull: false
      },
      data_cadastro: {
        type: Sequelize.DATEONLY,
        allowNull: false
      },
      orientacao: {
        type: Sequelize.STRING(30),
        allowNull: false
      },
      nome_social: {
        type: Sequelize.STRING(40),
        defaultValue: false
      },
      raca: {
        type: Sequelize.STRING(20),
        allowNull: false
      },
      deficiencia: {
        type: Sequelize.BOOLEAN,
        allowNull: false
      },
      descricao_deficiencia: {
        type: Sequelize.TEXT,
        allowNull: false
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('cliente');
  }
};
