'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('contrato_imobiliaria_cliente', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      id_cliente: {
        type: Sequelize.INTEGER(11),
        allowNull:false,
        references: { model: 'cliente' , key: 'id'}        
      },
      id_contrato_imobiliaria: {
        type: Sequelize.INTEGER(11),
        allowNull:false,
        references: { model: 'contrato_imobiliaria_proprietario', key: 'id'}
      },
      data_inicio: {
        allowNull:false,
        type: Sequelize.DATE
      },
      data_termino: {
        allowNull:false,
        type: Sequelize.DATE
      },
      valor_combinado: {
        allowNull:false,
        type: Sequelize.DECIMAL(10,0)
      },
      tipo: {
        allowNull:false,
        type: Sequelize.ENUM({
          values: ['aluguel', 'venda']
       })
      },
      fiador: {
        allowNull:false,
        type: Sequelize.STRING(100)
      },
      ativo: {
        allowNull:false,
        type: Sequelize.BOOLEAN
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('contrato_imobiliaria_cliente');
  }
};