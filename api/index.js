const express = require('express')
const rotas = require('./routes')
const port = 3000
const server = express()

rotas(server)

server.get('/', (req, res) => {
    return res.status(200).json({
        "mensagem": "API projeto imobiliária em execução"
    })
})

server.listen(port, () => {
    console.log(`Servidor rodando na porta ${port}`)
})

module.exports = server
