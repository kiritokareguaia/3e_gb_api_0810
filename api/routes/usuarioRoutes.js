const {Router} = require('express')
const routes = Router()
const usuariosController = require('../controllers/usuarioController')

routes.post('/usuarios', usuariosController.criarUmUsuario)
routes.post('/usuario/loggin', usuariosController.logar)
routes.get('/usuarios', usuariosController.buscarTodosOsUsuarios)
routes.get('/usuarios/completos', usuariosController.buscarTodosOsUsuariosCompletos)
routes.get('/usuarios/completos/pornome/:nome', usuariosController.buscarOsUsuariosCompletosPeloNome)
routes.patch('/usuarios/:id', usuariosController.atualizarUmUsuario)
routes.delete('/usuarios/:id', usuariosController.deletarUmUsuario)

module.exports = routes