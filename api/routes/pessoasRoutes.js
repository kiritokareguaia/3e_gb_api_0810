const { Router } = require ('express')
const routes = Router()
const pessoasContoller = require('../controllers/pessoasController')

routes.post('/pessoa', pessoasContoller.criarUmaPessoa)
routes.get('/pessoas', pessoasContoller.buscarTodasAsPessoas)
routes.get('/pessoas/nome/:nome', pessoasContoller.buscarPessoaPeloNome)
routes.get('/pessoas/nome/:nome/cidade/cidade/:cidade', pessoasContoller.buscarPessoaPeloNomeECidade)


routes.patch('/pessoas/:id', (req, res) => {
    const id = req.params['id']

    return res.status(200).json({
        "mensagem":`Vai atualizar a pessoa de acordo com o seguinte id "${id}"`
    })
})
routes.delete('/pessoas/:id', (req, res) => {
    const id = req.params['id']

    return res.status(200).json({
        "mensagem":`Vai apagar a pessoa de acordo com o id "${id}"`
    })
})


 module.exports = routes