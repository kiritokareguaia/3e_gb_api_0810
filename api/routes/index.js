const bodyParser= require('body-parser')
const pessoas = require('./pessoasRoute')
const usuarios = require('./usuariosRoute')
const imobiliarias = require('./imobiliariaRoute')
const imoveis = require('./imovelRoute')
const contratoImobiliariaClientes = require('./contrato_imobi_clienteRoute')
const contratoImobiliariaProprietario = require('./contrato_imobi_proprietarioRoute')

module.exports = (server) => {
    server.use(
        (req, res, next) => {
            res.header("Acces-Control-Allow-Origin", "*")
            res.header("Acces-Control-Allow-Methods", "GET, POST, PATH, DELETE")
            server.use(cors())
            next()

        }
        )

    server.use(bodyParser.json())
    server.use(pessoas)
    server.use(usuarios)
    server.use(imobiliarias)
    server.use(imoveis)
    server.use(contratoImobiliariaClientes)
    server.use(contratoImobiliariaProprietario)
}