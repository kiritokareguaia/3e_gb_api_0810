const {Router} = require('express')
const routes = Router()
const cont_imo_proprietarioController = require('../controllers/contrato_imobiliaria_proprietarioController')

routes.post('/cont_imo_proprietario', cont_imo_proprietarioController.novoProprietario)
routes.get('/cont_imo_proprietario', cont_imo_proprietarioController.bucarTodosOsContratosProprietarios)

routes.delete('/cont_imo_proprietario/:id', (req, res) => {
    const id = req.params['id']

    return res.status(200).json({
        "mensagem": `Deleta um contrato do proprietario de acordo com o id "${id}"`
    })
})

