const {Router} = require('express')

const routes = Router()

const cont_imo_clienteController = require('../controllers/contrato_imobiliaria_clienteController')

routes.post('/cont_imo_cliente', cont_imo_clienteController.criarUmContrato)
routes.get('/cont_imo_cliente', cont_imo_clienteController.buscarTodosOsContratos)
routes.delete('/cont_imo_cliente/:id', cont_imo_clienteController.deletarUmContrato)

routes.patch('/cont_imo_cliente/:id', (req, res) => {
    const id = req.params['id']

    return res.status(200).json({
        "mensagem":`Vai atualizar o contrato de acordo com o seguinte id "${id}"`
    })
})
routes.delete('/cont_imo_cliente/:id', (req, res) => {
    const id = req.params['id']

    return res.status(200).json({
        "mensagem":`Vai apagar o contrato de acordo com o id "${id}"`
    })
})


 module.exports = routes