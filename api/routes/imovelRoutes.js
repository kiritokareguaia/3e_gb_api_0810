const {Router} = require('express')
const imovelController = require('../controllers/imovelController')
const routes = Router()
const ImovelController = require('../controllers/imovelController')

routes.post('/imovel', imovelController.CriarImovel)
routes.get('/imovel', imovelController.bucarTodosOsImoveis)
routes.get('/imovel', imovelController.buscarTodosOsImoveisPelaCidade)
routes.delete('/imovel/:id', (req, res) => {
    const id = req.params['id']

    return res.status(200).json({
        "mensagem": `Vai apagar o imovel de acordo com o id "${id}"`
    })
})